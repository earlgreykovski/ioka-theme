# Logo + variants

## Full Color on Light
![[logo--fullcolor.png]]
Preference to use this only on pure white background, anything above brightness 90 (in a perceptual colorspace like OKLAB) is acceptable

## Full Color on Dark
![[logo--fullcolor-dark.png]]
Preference to use this only on pure black background, anything below brightness 10 (in a perceptual colorspace like OKLAB) is acceptable

## Solid Color
![[logo--solidcolor.png]]
The logo is a solid color on a smooth background that cas a sufficient contrast with the logo's color. The logo can be solid white, solid black, or any neutral color that matches the design.

The background must be +/-40 lighter or darker than the logo color (in a perceptual colorspace like OKLAB) AND must not be too busy so that the logo remains clear and legible.

Full color logos are preferred, but this is suitible for settings where the logo is secondary (such as in a grid of sponsors) or in settings where color rendering is not available.

## Icon Only
![[Pasted image 20220926171941.png]]
Same requirements as above, only to be used in cases where including the wordmark is impractical (such as favicons, app icons, etc)


# Colors

These values are designed to be in-keeping with the Material Design 3 color system that allows you to set key colors: *primary*, *secondary*, *tertiary*, *error*, and *neutral*.

Here are some excerpts that explain how these colors might be used in the context of an interface:

> The primary key color is used to derive roles for key components across the UI, such as the FAB, prominent buttons, active states, as well as the tint of elevated surfaces.

> The secondary key color is used for less prominent components in the UI such as filter chips, while expanding the opportunity for color expression.

> The tertiary key color is used to derive the roles of contrasting accents that can be used to balance primary and secondary colors or bring heightened attention to an element. The tertiary color role is left for teams to use at their discretion and is intended to support broader color expression in products.

> The neutral key color is used to derive the roles of surface and background

> Error colors: In addition to the accent and neutral key color, the color system includes a semantic color role for error

> A tonal palette consists of thirteen tones, including white and black. A tone value of 100 is equivalent to the idea of light at its maximum and results in white. Every tone value between 0 and 100 expresses the amount of light present in the color.

In a departure from the material design system, I have created all of the tonal palette color values derived from the key colors here using the OKHSL color space (https://bottosson.github.io/posts/colorpicker/) which is build on the perceptually correct OKLAB color space that is soon to be a supported in-browser color space (already live in safari). Google's new color space, HCT, produces in my opinion oversaturated/garish colors, but they are not so substantially different that they would not match. These "extended" tonal palettes can be considered a useful tool rather than any sort of strict definition.

 - <span style="background:#000000; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*black:* rgb: (0, 0, 0) • hex: #000000 • OKHSL: (0, 0, 100)
 - <span style="background:#ffffff; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*white:* rgb: (255, 255, 255) • hex: #ffffff • OKHSL: (0, 0, 0)

 - <span style="background:#33A3DC; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary:* rgb: (51, 163, 220) • hex: #33A3DC • OKHSL: (236, 85, 63)
     - <span style="background:#ffffff; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-100:* rgb: (255, 255, 255) • hex: #ffffff • OKHSL: (236, 85, 100)
     - <span style="background:#f4faff; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-98:* rgb: (244, 250, 255) • hex: #f4faff • OKHSL: (236, 85, 98)
     - <span style="background:#e3f4fe; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-95:* rgb: (227, 244, 254) • hex: #e3f4fe • OKHSL: (236, 85, 95)
     - <span style="background:#c7e8fd; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-90:* rgb: (199, 232, 253) • hex: #c7e8fd • OKHSL: (236, 85, 90)
     - <span style="background:#8bd0fb; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-80:* rgb: (139, 208, 251) • hex: #8bd0fb • OKHSL: (236, 85, 80)
     - <span style="background:#4fb7f0; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-70:* rgb: (79, 183, 240) • hex: #4fb7f0 • OKHSL: (236, 85, 70)
     - <span style="background:#2c9cd3; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-60:* rgb: (44, 156, 211) • hex: #2c9cd3 • OKHSL: (236, 85, 60)
     - <span style="background:#1c80b0; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-50:* rgb: (28, 128, 176) • hex: #1c80b0 • OKHSL: (236, 85, 50)
     - <span style="background:#13658d; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-40:* rgb: (19, 101, 141) • hex: #13658d • OKHSL: (236, 85, 40)
     - <span style="background:#0c4b6a; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-30:* rgb: (12, 75, 106) • hex: #0c4b6a • OKHSL: (236, 85, 30)
     - <span style="background:#053248; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-20:* rgb: (83, 36, 8) • hex: #053248 • OKHSL: (236, 85, 20)
     - <span style="background:#021926; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-10:* rgb: (33, 146, 6) • hex: #021926 • OKHSL: (236, 85, 10)
     - <span style="background:#000000; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*primary-0:* rgb: (0, 0, 0) • hex: #000000 • OKHSL: (236, 85, 0)

 - <span style="background:#88C6C4; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary:* rgb: (136, 198, 196) • hex: #88C6C4 • OKHSL: (193, 43, 75)
     - <span style="background:#ffffff; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-100:* rgb: (255, 255, 255) • hex: #ffffff • OKHSL: (193, 43, 100)
     - <span style="background:#f4fbfb; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-98:* rgb: (244, 251, 251) • hex: #f4fbfb • OKHSL: (193, 43, 98)
     - <span style="background:#e4f5f4; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-95:* rgb: (228, 245, 244) • hex: #e4f5f4 • OKHSL: (193, 43, 95)
     - <span style="background:#c8ebe9; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-90:* rgb: (200, 235, 233) • hex: #c8ebe9 • OKHSL: (193, 43, 90)
     - <span style="background:#9ad3d1; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-80:* rgb: (154, 211, 209) • hex: #9ad3d1 • OKHSL: (193, 43, 80)
     - <span style="background:#79b9b7; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-70:* rgb: (121, 185, 183) • hex: #79b9b7 • OKHSL: (193, 43, 70)
     - <span style="background:#609e9b; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-60:* rgb: (96, 158, 155) • hex: #609e9b • OKHSL: (193, 43, 60)
     - <span style="background:#4c8280; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-50:* rgb: (76, 130, 128) • hex: #4c8280 • OKHSL: (193, 43, 50)
     - <span style="background:#396766; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-40:* rgb: (57, 103, 102) • hex: #396766 • OKHSL: (193, 43, 40)
     - <span style="background:#294d4c; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-30:* rgb: (41, 77, 76) • hex: #294d4c • OKHSL: (193, 43, 30)
     - <span style="background:#193333; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-20:* rgb: (25, 51, 51) • hex: #193333 • OKHSL: (193, 43, 20)
     - <span style="background:#0a1919; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-10:* rgb: (10, 25, 25) • hex: #0a1919 • OKHSL: (193, 43, 10)
     - <span style="background:#000000; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*secondary-0:* rgb: (0, 0, 0) • hex: #000000 • OKHSL: (193, 43, 0)

 - <span style="background:#FBC58D; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary:* rgb: (251, 197, 141) • hex: #FBC58D • OKHSL: (67, 83, 84)
     - <span style="background:#ffffff; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-100:* rgb: (255, 255, 255) • hex: #ffffff • OKHSL: (67, 83, 100)
     - <span style="background:#fff8f2; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-98:* rgb: (255, 248, 242) • hex: #fff8f2 • OKHSL: (67, 83, 98)
     - <span style="background:#feeede; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-95:* rgb: (254, 238, 222) • hex: #feeede • OKHSL: (67, 83, 95)
     - <span style="background:#fddcbb; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-90:* rgb: (253, 220, 187) • hex: #fddcbb • OKHSL: (67, 83, 90)
     - <span style="background:#f9b873; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-80:* rgb: (249, 184, 115) • hex: #f9b873 • OKHSL: (67, 83, 80)
     - <span style="background:#e6983b; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-70:* rgb: (230, 152, 59) • hex: #e6983b • OKHSL: (67, 83, 70)
     - <span style="background:#c67f25; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-60:* rgb: (198, 127, 37) • hex: #c67f25 • OKHSL: (67, 83, 60)
     - <span style="background:#a4681b; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-50:* rgb: (164, 104, 27) • hex: #a4681b • OKHSL: (67, 83, 50)
     - <span style="background:#825213; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-40:* rgb: (130, 82, 19) • hex: #825213 • OKHSL: (67, 83, 40)
     - <span style="background:#623c0b; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-30:* rgb: (98, 60, 11) • hex: #623c0b • OKHSL: (67, 83, 30)
     - <span style="background:#422705; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-20:* rgb: (66, 39, 5) • hex: #422705 • OKHSL: (67, 83, 20)
     - <span style="background:#221202; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-10:* rgb: (34, 18, 2) • hex: #221202 • OKHSL: (67, 83, 10)
     - <span style="background:#000000; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*tertiary-0:* rgb: (0, 0, 0) • hex: #000000 • OKHSL: (67, 83, 0)

 - <span style="background:#BA1A1A; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error:* rgb: (186, 26, 26) • hex: #BA1A1A • OKHSL: (28, 96, 43)
     - <span style="background:#ffffff; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-100:* rgb: (255, 255, 255) • hex: #ffffff • OKHSL: (28, 96, 100)
     - <span style="background:#fff7f6; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-98:* rgb: (255, 247, 246) • hex: #fff7f6 • OKHSL: (28, 96, 98)
     - <span style="background:#ffece9; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-95:* rgb: (255, 236, 233) • hex: #ffece9 • OKHSL: (28, 96, 95)
     - <span style="background:#fed9d3; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-90:* rgb: (254, 217, 211) • hex: #fed9d3 • OKHSL: (28, 96, 90)
     - <span style="background:#feb0a5; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-80:* rgb: (254, 176, 165) • hex: #feb0a5 • OKHSL: (28, 96, 80)
     - <span style="background:#fd8274; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-70:* rgb: (253, 130, 116) • hex: #fd8274 • OKHSL: (28, 96, 70)
     - <span style="background:#f8493e; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-60:* rgb: (248, 73, 62) • hex: #f8493e • OKHSL: (28, 96, 60)
     - <span style="background:#d82421; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-50:* rgb: (216, 36, 33) • hex: #d82421 • OKHSL: (28, 96, 50)
     - <span style="background:#ae1817; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-40:* rgb: (174, 24, 23) • hex: #ae1817 • OKHSL: (28, 96, 40)
     - <span style="background:#840f0e; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-30:* rgb: (132, 15, 14) • hex: #840f0e • OKHSL: (28, 96, 30)
     - <span style="background:#5b0707; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-20:* rgb: (91, 7, 7) • hex: #5b0707 • OKHSL: (28, 96, 20)
     - <span style="background:#310202; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-10:* rgb: (49, 2, 2) • hex: #310202 • OKHSL: (28, 96, 10)
     - <span style="background:#000000; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*error-0:* rgb: (0, 0, 0) • hex: #000000 • OKHSL: (28, 96, 0)

 - <span style="background:#7f9db0; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral:* rgb: (127, 157, 176) • hex: #7f9db0 • OKHSL: (236, 25, 63)
     - <span style="background:#ffffff; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-100:* rgb: (255, 255, 255) • hex: #ffffff • OKHSL: (236, 25, 100)
     - <span style="background:#f7fafb; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-98:* rgb: (247, 250, 251) • hex: #f7fafb • OKHSL: (236, 25, 98)
     - <span style="background:#ecf2f5; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-95:* rgb: (236, 242, 245) • hex: #ecf2f5 • OKHSL: (236, 25, 95)
     - <span style="background:#d9e5ec; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-90:* rgb: (217, 229, 236) • hex: #d9e5ec • OKHSL: (236, 25, 90)
     - <span style="background:#b5cad8; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-80:* rgb: (181, 202, 216) • hex: #b5cad8 • OKHSL: (236, 25, 80)
     - <span style="background:#94b0c2; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-70:* rgb: (148, 176, 194) • hex: #94b0c2 • OKHSL: (236, 25, 70)
     - <span style="background:#7896a9; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-60:* rgb: (120, 150, 169) • hex: #7896a9 • OKHSL: (236, 25, 60)
     - <span style="background:#5f7b8d; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-50:* rgb: (95, 123, 141) • hex: #5f7b8d • OKHSL: (236, 25, 50)
     - <span style="background:#4a6271; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-40:* rgb: (74, 98, 113) • hex: #4a6271 • OKHSL: (236, 25, 40)
     - <span style="background:#354955; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-30:* rgb: (53, 73, 85) • hex: #354955 • OKHSL: (236, 25, 30)
     - <span style="background:#223039; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-20:* rgb: (34, 48, 57) • hex: #223039 • OKHSL: (236, 25, 20)
     - <span style="background:#0f181d; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-10:* rgb: (15, 24, 29) • hex: #0f181d • OKHSL: (236, 25, 10)
     - <span style="background:#000000; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*neutral-0:* rgb: (0, 0, 0) • hex: #000000 • OKHSL: (236, 25, 0)

 - <span style="background:#989898; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey:* rgb: (152, 152, 152) • hex: #989898 • OKHSL: (0, 0, 63)
     - <span style="background:#ffffff; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-100:* rgb: (255, 255, 255) • hex: #ffffff • OKHSL: (0, 0, 100)
     - <span style="background:#f9f9f9; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-98:* rgb: (249, 249, 249) • hex: #f9f9f9 • OKHSL: (0, 0, 98)
     - <span style="background:#f1f1f1; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-95:* rgb: (241, 241, 241) • hex: #f1f1f1 • OKHSL: (0, 0, 95)
     - <span style="background:#e2e2e2; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-90:* rgb: (226, 226, 226) • hex: #e2e2e2 • OKHSL: (0, 0, 90)
     - <span style="background:#c7c7c7; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-80:* rgb: (199, 199, 199) • hex: #c7c7c7 • OKHSL: (0, 0, 80)
     - <span style="background:#ababab; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-70:* rgb: (171, 171, 171) • hex: #ababab • OKHSL: (0, 0, 70)
     - <span style="background:#919191; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-60:* rgb: (145, 145, 145) • hex: #919191 • OKHSL: (0, 0, 60)
     - <span style="background:#777777; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-50:* rgb: (119, 119, 119) • hex: #777777 • OKHSL: (0, 0, 50)
     - <span style="background:#5e5e5e; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-40:* rgb: (94, 94, 94) • hex: #5e5e5e • OKHSL: (0, 0, 40)
     - <span style="background:#464646; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-30:* rgb: (70, 70, 70) • hex: #464646 • OKHSL: (0, 0, 30)
     - <span style="background:#2e2e2e; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-20:* rgb: (46, 46, 46) • hex: #2e2e2e • OKHSL: (0, 0, 20)
     - <span style="background:#161616; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-10:* rgb: (22, 22, 22) • hex: #161616 • OKHSL: (0, 0, 10)
     - <span style="background:#000000; display:inline-block; width:1em; height:1em; vertical-align:middle; margin-right:.5em;"></span>*grey-0:* rgb: (0, 0, 0) • hex: #000000 • OKHSL: (0, 0, 0)

## As CSS variables
The trick here, borrowed from tailwind css, is that they need to be wrapped inside of the rgb color function in the form of ```rgb(var(--color--primary))```. this Allows you to generate rgba colors with opacity in the form of ```rgb(var(--color--primary) / var(--opacity))```

```
--color--black: 0 0 0 /* #000 */;
--color--white: 255 255 255 /* #FFF */;
      
--color--primary: 51 163 220 /* #33A3DC */;
    --color--primary-100: 255 255 255 /* #ffffff */;
    --color--primary-98: 244 250 255 /* #f4faff */;
    --color--primary-95: 227 244 254 /* #e3f4fe */;
    --color--primary-90: 199 232 253 /* #c7e8fd */;
    --color--primary-80: 139 208 251 /* #8bd0fb */;
    --color--primary-70: 79 183 240 /* #4fb7f0 */;
    --color--primary-60: 44 156 211 /* #2c9cd3 */;
    --color--primary-50: 28 128 176 /* #1c80b0 */;
    --color--primary-40: 19 101 141 /* #13658d */;
    --color--primary-30: 12 75 106 /* #0c4b6a */;
    --color--primary-20: 83 36 8 /* #053248 */;
    --color--primary-10: 33 146 6 /* #021926 */;
    --color--primary-0: 0 0 0 /* #000000 */;
      
--color--secondary: 136 198 196 /* #88C6C4 */;
    --color--secondary-100: 255 255 255 /* #ffffff */;
    --color--secondary-98: 244 251 251 /* #f4fbfb */;
    --color--secondary-95: 228 245 244 /* #e4f5f4 */;
    --color--secondary-90: 200 235 233 /* #c8ebe9 */;
    --color--secondary-80: 154 211 209 /* #9ad3d1 */;
    --color--secondary-70: 121 185 183 /* #79b9b7 */;
    --color--secondary-60: 96 158 155 /* #609e9b */;
    --color--secondary-50: 76 130 128 /* #4c8280 */;
    --color--secondary-40: 57 103 102 /* #396766 */;
    --color--secondary-30: 41 77 76 /* #294d4c */;
    --color--secondary-20: 25 51 51 /* #193333 */;
    --color--secondary-10: 10 25 25 /* #0a1919 */;
    --color--secondary-0: 0 0 0 /* #000000 */;
      
--color--tertiary: 251 197 141 /* #FBC58D */;
    --color--tertiary-100: 255 255 255 /* #ffffff */;
    --color--tertiary-98: 255 248 242 /* #fff8f2 */;
    --color--tertiary-95: 254 238 222 /* #feeede */;
    --color--tertiary-90: 253 220 187 /* #fddcbb */;
    --color--tertiary-80: 249 184 115 /* #f9b873 */;
    --color--tertiary-70: 230 152 59 /* #e6983b */;
    --color--tertiary-60: 198 127 37 /* #c67f25 */;
    --color--tertiary-50: 164 104 27 /* #a4681b */;
    --color--tertiary-40: 130 82 19 /* #825213 */;
    --color--tertiary-30: 98 60 11 /* #623c0b */;
    --color--tertiary-20: 66 39 5 /* #422705 */;
    --color--tertiary-10: 34 18 2 /* #221202 */;
    --color--tertiary-0: 0 0 0 /* #000000 */;
      
--color--error: 186 26 26 /* #BA1A1A */;
    --color--error-100: 255 255 255 /* #ffffff */;
    --color--error-98: 255 247 246 /* #fff7f6 */;
    --color--error-95: 255 236 233 /* #ffece9 */;
    --color--error-90: 254 217 211 /* #fed9d3 */;
    --color--error-80: 254 176 165 /* #feb0a5 */;
    --color--error-70: 253 130 116 /* #fd8274 */;
    --color--error-60: 248 73 62 /* #f8493e */;
    --color--error-50: 216 36 33 /* #d82421 */;
    --color--error-40: 174 24 23 /* #ae1817 */;
    --color--error-30: 132 15 14 /* #840f0e */;
    --color--error-20: 91 7 7 /* #5b0707 */;
    --color--error-10: 49 2 2 /* #310202 */;
    --color--error-0: 0 0 0 /* #000000 */;
      
--color--neutral: 127 157 176 /* #7f9db0 */;
    --color--neutral-100: 255 255 255 /* #ffffff */;
    --color--neutral-98: 247 250 251 /* #f7fafb */;
    --color--neutral-95: 236 242 245 /* #ecf2f5 */;
    --color--neutral-90: 217 229 236 /* #d9e5ec */;
    --color--neutral-80: 181 202 216 /* #b5cad8 */;
    --color--neutral-70: 148 176 194 /* #94b0c2 */;
    --color--neutral-60: 120 150 169 /* #7896a9 */;
    --color--neutral-50: 95 123 141 /* #5f7b8d */;
    --color--neutral-40: 74 98 113 /* #4a6271 */;
    --color--neutral-30: 53 73 85 /* #354955 */;
    --color--neutral-20: 34 48 57 /* #223039 */;
    --color--neutral-10: 15 24 29 /* #0f181d */;
    --color--neutral-0: 0 0 0 /* #000000 */;
      
--color--grey: 152 152 152 /* #989898 */;
    --color--grey-100: 255 255 255 /* #ffffff */;
    --color--grey-98: 249 249 249 /* #f9f9f9 */;
    --color--grey-95: 241 241 241 /* #f1f1f1 */;
    --color--grey-90: 226 226 226 /* #e2e2e2 */;
    --color--grey-80: 199 199 199 /* #c7c7c7 */;
    --color--grey-70: 171 171 171 /* #ababab */;
    --color--grey-60: 145 145 145 /* #919191 */;
    --color--grey-50: 119 119 119 /* #777777 */;
    --color--grey-40: 94 94 94 /* #5e5e5e */;
    --color--grey-30: 70 70 70 /* #464646 */;
    --color--grey-20: 46 46 46 /* #2e2e2e */;
    --color--grey-10: 22 22 22 /* #161616 */;
    --color--grey-0: 0 0 0 /* #000000 */;
```

## Color Accessibility Guidelines 

See Material Design 3 guidelines: https://m3.material.io/styles/color/the-color-system/accessibility

The most important consideration here is making sure that there is sufficient contrast between adjacent colors to account for visual impairments (color blindness and low vision). The minimum requirement is a contrast ratio of 3:1, which can be attained by using colors that are +/- 40 units apart in lightness value:

<div style="background: #5f7b8d; color:#c8ebe9; padding: 2em; text-align: center">secondary-90 on neutral-50: Acceptable</div>
<div style="background: #c7e8fd; color:#e6983b; padding: 2em; text-align: center">tertiary-70 on primary-90: Not Enough Contrast</div>

# Type / Grid System

## Fonts & sizes

This system is built around open source fonts, to reduce the technical and financial burdens associated with using traditional licensed fonts. 

The Basic family used for most applications is Google's 'Roboto', which comes in many variants. 

I have also parametric font called 'Fraunces' as a more flashy display font: typically used in italic with it's "SOFT" variable set to 35, at either fairly thin weight <200 or heavy >600). 

When appropriate, substitute Roboto for the matching native system fonts, to improve performance and match the "native" appearance of most apps.

```
--font--sans: system-ui, Roboto, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
--font--serif: 'fraunces', Iowan Old Style, Apple Garamond, Baskerville, Times New Roman, Droid Serif, Times, Source Serif Pro, serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;
--font--mono: Roboto Mono, Menlo, Consolas, Monaco, Liberation Mono, Lucida Console, monospace;
```

I have used a type scale set to √((1 + √5)/2), which is the square root of the "golden ratio" which often shows up in designs. 

A type scale ensures that the relative size of the fonts grows in a perceptually uniform way. If you have a base font size, ```$base```, the next size up will be ```$base * $type-scale```, the next size up after that will be ```$base * $type-scale^2```, etc. You can also generate smaller sizes, as I have done here by dividing by that number: ```$base / $type-scale```, ```$base / $type-scale^2```. 

For ease of use, I have rounded these numbers to nearby round values (the nearest .125, .25, .5, or eventually the nearest full interger as the scale gets larger). 

```
font-size: 16px; /* sets the rem size */
    
--font-size--xs: 0.625rem;  /* 10px */
--font-size--sm: 0.75rem;   /* 12px */
--font-size--base: 1rem;    /* 16px */
--font-size--lg: 1.25rem;   /* 20px */
--font-size--xl: 1.5rem;    /* 24px */
--font-size--2xl: 2rem;     /* 32px */
--font-size--3xl: 2.5rem;   /* 40px */
--font-size--4xl: 3.25rem;  /* 52px */
--font-size--5xl: 4.25rem;  /* 68px */
--font-size--6xl: 5.5rem;   /* 88px */
--font-size--7xl: 7rem;     /* 112px */
--font-size--8xl: 9rem;     /* 144px */
```


the line heights also scale geometrically, but at a slightly smaller factor (1.2), with a minimum value of the font-size. These are also aligned with the round numbers that form the *baseline grid*.

```
--font-lh--xs: 1rem;      /* 16px */
--font-lh--sm: 1.25rem;   /* 20px */
--font-lh--base: 1.5rem;  /* 24px */
--font-lh--lg: 1.75rem;   /* 28px */
--font-lh--xl: 2.25rem;   /* 36px */
--font-lh--2xl: 2.5rem;   /* 40px */
--font-lh--3xl: 3rem;     /* 48px */
--font-lh--4xl: 3.75rem;  /* 60px */
--font-lh--5xl: 4.5rem;   /* 72px */
--font-lh--6xl: 5.5rem;   /* 88px */
--font-lh--7xl: 7rem;     /* 112px */
--font-lh--8xl: 9rem;     /* 144px */
```

The baseline grid is the grid that the design follows on the vertical axis, and is usually the same as the line height of the base font size. In this case, it is .25rem (which defaults to 4px), which is 1/4 of the smallest font size (or 1/6 of the 'base' font size);

Some design systems also define a "spacing scale", such that the amount of spacing/padding you can add to things scales up geometrically similar to the font sizes. The following is provided as a convenience, but any round number of rems would be compatible with the baseline grid:

```
--spacing--xs: 0.5rem;    /* 8px */
--spacing--sm: 0.75rem;   /* 12px */
--spacing--base: 1rem;    /* 16px */
--spacing--lg: 1.25rem;   /* 20px */
--spacing--xl: 1.5rem;    /* 24px */
--spacing--2xl: 2rem;     /* 32px */
--spacing--3xl: 2.5rem;   /* 40px */
--spacing--4xl: 3.25rem;  /* 52px */
--spacing--5xl: 4rem;     /* 64px */
--spacing--6xl: 5.5rem;   /* 88px */
--spacing--7xl: 7rem;     /* 112px */
--spacing--8xl: 8.5rem;   /* 136px */
--spacing--9xl: 11rem;    /* 176px */
--spacing--10xl: 14rem;   /* 224px */
```

Example:
<p style="font-size: 144px; line-height: 144px; margin:0 0 1rem;">Font Size: 8xl</p>
<p style="font-size: 112px; line-height: 112px; margin:0 0 1rem;">Font Size: 7xl</p>
<p style="font-size: 88px; line-height: 88px; margin:0 0 1rem;">Font Size: 6xl</p>
<p style="font-size: 68px; line-height: 72px; margin:0 0 1rem;">Font Size: 5xl</p>
<p style="font-size: 52px; line-height: 60px; margin:0 0 1rem;">Font Size: 4xl</p>
<p style="font-size: 40px; line-height: 48px; margin:0 0 1rem;">Font Size: 3xl</p>
<p style="font-size: 32px; line-height: 40px; margin:0 0 1rem;">Font Size: 2xl</p>
<p style="font-size: 24px; line-height: 36px; margin:0 0 1rem;">Font Size: xl</p>
<p style="font-size: 20px; line-height: 28px; margin:0 0 1rem;">Font Size: lg</p>
<p style="font-size: 16px; line-height: 24px; margin:0 0 1rem;">Font Size: base</p>
<p style="font-size: 12px; line-height: 20px; margin:0 0 1rem;">Font Size: sm</p>
<p style="font-size: 10px; line-height: 16px; margin:0 0 1rem;">Font Size: xs</p>

## Horizontal Grid

I find it useful to divide the design horizontally into a 14 column grid: a standard 12 column grid with a maximum width of 74rem, and a flexible column on either side that takes up the remaining space (or collapses to nothing if the width of the display is below 74rem).

![[Pasted image 20220926164537.png]]

A 12 column grid allows you to comfortably divide the horizontal space into 1/2, 1/3, 1/4, and 1/6. By including the flexible "bonus" columns, it becomes easy to include elements that span beyond the central layout to the edge of the display.

```
grid-template-columns: calc((100vw - 74rem) / 2) repeat(12, 1fr) calc((100vw - 74rem) / 2); 
gap: 0px min(2rem, 7vw);
```

At desktop sizes this gives each column a width of 4rem and a gutter of 2rem on either side.

A column of text should never be wider than 45x its text size (or "45em", as opposed to rem) or it will cause eye strain. In practice, this means that 'base' sized text will never span more than 7 columns unless you are at mobile sizes. This is also why the width of the inner grid is capped at 74rem.



# Secondary Graphics
## AI Art Style
We are only beginning to explore the use of AI art generation tools used within a design system. in the future images will be able to be reliably produced via a single api call, as of right now it is more of a creative process. I will document the generation of the hero image of the homepage here as a case study:

Using the prompt "a sunset over a mountain range in the pacific northwest, by Caspar David Friedrich, digital art, matte painting trending on artstation HQ 4k" in DALLE-2, I generated the following image:

![[DALL·E 2022-09-20 15.42.31 - a sunset over a mountain range in the pacific northwest, by Caspar David Friedrich, digital art, matte painting trending on artstation HQ 4k.png]]

Breaking that down
- "a sunset over a mountain range in the pacific northwest" reflects the subject matter
- "Caspar David Friedrich" was a 19th-century German Romantic landscape painter. His primary interest was the contemplation of nature, and his often symbolic and anti-classical work seeks to convey a subjective, emotional response to the natural world
- "digital art, matte painting trending on artstation HQ 4k" are style cues intended to trick the AI into outputting higher quality output. "digital art" in particular is probably responsible for the lack of visible paint strokes

I used the same prompt with DALLE-2's "outpainting" feature:

![[DALL·E 2022-09-20 15.54.38 - a sunset over a mountain range in the pacific northwest, by Caspar David Friedrich, digital art, matte painting trending on artstation HQ 4k.png]]

I flipped the image horizontally, and edited the colors to more closely match the ioka color scheme:

![[mountains2.jpg]]

I fed a small section of the image, the actual sunset area, into stable diffusion's img2img tool with the same prompt at a weight of around .85.

![[769823201_a_sunset_over_a_mountain_range_in_the_pacific_northwest__by_Caspar_David_Friedrich__matte_painting_trending_on_artstation_HQ.png]]

I reincorporated this into the original image using DALLE-2's image inpainting tool and some minor manual digital painting in photoshop. I fed it through another AI super-resolution algorithm, did some more manual editing, and ended up with this as a final result.

![[abstract-mountain2.jpg]]

If I were trying to generate a similar image, i may include certain emotional keywords: to reflect this style: smooth, dreamlike, contemplative, inspiring.

In the future it may be possible to make use of textual inversion to encode certain style concepts and the ioka color scheme to more readily generate usable images in this style: https://textual-inversion.github.io/

## Rounded Hexagon / "Building Block" Motif
The Ioka logo is a slightly rounded hexagon, so we make use of it as a recurring motif in the design.

Examples:
![[Pasted image 20220926161631.png]]

 ![[Pasted image 20220926161702.png]]

these containing elements use sections of rounded hexagons

![[Pasted image 20220926161757.png]]

![[Pasted image 20220926161810.png]]

these elements are masked inside of rounded hexagons

![[Pasted image 20220926161854.png]]

This is an isomorphic cube, or "building block" with subtle outlines of rounded hexagons in the background. The "building block" itself is a rounded hexagon, which implies that all of the rounded hexagons are the sillohuttes of these blocks. 

![[Pasted image 20220926162544.png]]

The subtle semantic meaning of these building blocks is that they represent all of the people and technologies that come together to form the systems and communities that Ioka builds. This construction, dubbed "the lattice", represents that system overall.

## Angled Dividers
Each section of the homepage is divided at a 4.7° angle to provide some visual interest.

![[Pasted image 20220926163554.png]]

as an alternative to hegagon based containers (if the design is beginning to look repetitive) you can contain elements inside of a 4.7° paralellogram:

![[Pasted image 20220926163743.png]]

# Icons

See Material Design 3 guidelines: https://m3.material.io/styles/icons/overview

We use the rounded + filled variant, at a weight of 400:

![[Pasted image 20220926163822.png]]
